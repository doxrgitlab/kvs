# KVS
## Kernel Version Switcher

note that kxtz may re-release soon

### What is KVS?
KVS is a side project of mine that will change your current tpm_kernver to ANY version between 0 and 3.

This works using the hexdumps of 0x1008 (kernver TPM index) and tpmc to write the hexdumps of your selected kernver to the 0x1008 TPM index.

### How do I use this?
To use KVS, you must download your shim from kxtz' shim mirror

After downloading, flash your USB/SD with the file, I recommend Chrome Recovery Utility.

### FAQ
Q: Will this brick my device?

A: No, KVS has no way to permanantly brick / hard brick a device. At most, it will make ChromeOS stop booting, but you will still be able to boot shims to recover the kernver index.

Q: What kernvers are there?
A: There are 4 valid kernvers, 0, 1, 2, and 3.

Q: What versions can I downgrade to with kernver _
A: Heres what versions you can downgrade to with each kernver

kernver 0: any

kernver 1: any

kernver 2: 112 - 119

kernver 3: 120 - latest

Q: What is the difference between kernver 0 and kernver 1?
A: Both kernver 0 and kernver 1 can downgrade to any version, but kernver 0 doesn't get overriden if you recover to a newer version.

### Credits
kxtzownsu - Writing KVS, Providing kernver 0 & kernver 1 files.

planetearth1363 - Providing kernver 2 files

miimaker - Providing kernver 3 files

OlyB - Helping me with the shim builder, most of the shim builder wouldn't exist without him.

Google - Writing the tpmc command :3

